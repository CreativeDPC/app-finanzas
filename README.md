
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

Prototipo de aplicación de finanzas.    


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn run start


```



## Tecnologías Utilizadas

- [React Js](https://es.reactjs.org/) (Javascript)


## Prueba en vivo

[App Finanzas](https://app-finanzas-34567.web.app)

## Imagenes

![Home](https://bitbucket.org/CreativeDPC/app-finanzas/raw/d9fb56bb487fa2a0ca1bb498462ce5f3919bd574/screens/home.png)

![Disposición](https://bitbucket.org/CreativeDPC/app-finanzas/raw/d9fb56bb487fa2a0ca1bb498462ce5f3919bd574/screens/disposicion.png)
