export default  [
    {
        img: "auto.png",
        name: "Auto",
        category: "Mis logros",
        date: "2m",
        amount: "$350,000.00"
    },
    {
        img: "cfe.png",
        name: "CFE",
        category: "Pago de Servicio",
        date: "Ayer",
        amount: "$280.00"
    },
    {
        img: "netflix.png",
        name: "NETFLIX",
        category: "Suscripciones",
        date: "28 de dic",
        amount: "$140.00"
    },
    {
        img: "mercado.png",
        name: "Supermercado",
        category: "Soriana Parque Delta",
        date: "26 de dic",
        amount: "$2,800.00"
    },            
];
