import React from "react";
import { Actions } from "../components/common/Actions";
import { AvailableBalance } from "../components/common/AvailableBalance";
import { Payment } from "../components/common/Payment";
import {Transactions} from '../components/common/Transactions'

export const HomeView = ({setNavBar}) => {
    
    setNavBar(true);

    return (
        <div className="home-container">
            <div className="region-user-info">
                <h1>
                    Hola José
                    <img src={'./assets/images/avatar.png'} />
                </h1>
            </div>
            <AvailableBalance />
            <Actions />
            <Payment />
            <Transactions />
        </div>
    )
}
