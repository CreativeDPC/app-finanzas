import React from "react";
import { Navigation } from "../components/common/Navigation";
import { Withdrawal } from "../components/common/Withdrawal";

export const WithdrawalView = ({setNavBar}) => {
    setNavBar(false);
    return (
        <div className="withdrawalview-container">
            <Navigation />
            <Withdrawal />
        </div>
    )
}
