import React, { useState } from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from 'react-router-dom';
//import iphone from '../../public/assets/images/iphone-border.png';
import { Header } from '../components/layout/Header';
import { NavBar } from '../components/layout/NavBar';
import { HomeView } from '../views/HomeView';
import { WithdrawalView } from '../views/WithdrawalView';

export const AppRouter = () => {
    
    const [showNavBar, setShowNavBar] = useState(true);

    return (
        <Router>
            <div className="principal-container">
                {/* <img src={`./assets/images/iphone-border.png`} /> */}
                <Header />
                <Switch>
                    <Route exact path="/withdrawal" render={() => <WithdrawalView setNavBar={setShowNavBar} />} /> 
                    <Route exact path="/" render={() => <HomeView setNavBar={setShowNavBar} />} />
                    <Redirect to="/" />
                </Switch>
                <NavBar showActions={showNavBar} />
            </div>
        </Router>
    )
}
