import { Link } from 'react-router-dom';

export const NavBar = ({showActions}) => {
    return (
        <div className="navbar-container">
            {
                showActions &&
                <div className="actions-navbar">
                    <Link to={`./withdrawal`}>
                        <i className="far fa-home-lg-alt"></i>
                    </Link>
                    <Link to={`./withdrawal`}>
                        <i className="fas fa-sack-dollar"></i>
                    </Link>
                    <Link to={`./withdrawal`}>
                        <i className="fas fa-credit-card"></i>
                    </Link>
                    <Link to={`./withdrawal`}>
                        <i className="fas fa-star"></i>
                    </Link>
                    <Link to={`./withdrawal`}>
                        <i className="fas fa-bell"></i>
                    </Link>
                </div>                 
            }
            <hr />
        </div>
    );
}