import React from "react";

export const Actions = () => {
    return (
        <div className="actions-container">
            <button>
                <i className="fas fa-star"></i>
                Crear logro
            </button>
            <button>
                <i className="fas fa-tag"></i>
                Pagar Servicio
            </button>
            <button>
                <i className="fas fa-bell"></i>
                Suscripción
            </button>
        </div>
    )
}
