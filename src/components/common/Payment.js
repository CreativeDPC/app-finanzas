import React from "react";

export const Payment = () => {
    return (
        <div className="payment-container">
            <div className="card">
                <h2>Tu próximo pago</h2>
                <span className="amount">$28,860.00</span>
                <div id="region-payment">
                    <div>
                        <h2>Fecha de pago</h2>
                        <span>16 Enero</span>
                    </div>
                    <div>
                        <button>
                            PAGAR AHORA 
                        </button>
                    </div>
                </div>
                <div id="region-info">
                    <div>
                        <span><i className="fas fa-sack-dollar"></i>Mis Logros</span>
                        <b>$27,260.00</b>    
                    </div>
                    <div>
                        <span><i className="fas fa-credit-card"></i>Compras con TDC</span>
                        <b>$1,600.00</b>    
                    </div>
                </div>
            </div>
        </div>
    )
}
