import React from "react";
import { AvailableBalance } from "./AvailableBalance";

export const Withdrawal = () => {
    return (
        <div className="withdrawal-container">
            <h1>Disposición</h1>
            <AvailableBalance />
            <form>
                <input type="text" defaultValue="Auto"/>
                <input type="text" defaultValue="$350,000.00"/>
                <p>Selecciona tu plazo</p>
                <div className="region-graph">
                    <div className="donut" ></div>
                    <div className="donut2" ></div>
                    <div className="donut3">
                        <div className="start"></div>
                        <div className="end"></div>
                    </div>
                    <div className="data">
                        <p>Tu pago mensual</p>
                        <h2>$12,000.00</h2>
                        <p>a 60 meses</p>
                        <p className="blue">Tasa de interés</p>
                        <p className="blue">16.6%</p>
                    </div>
                </div>
                <button>
                    LO QUIERO
                </button>
            </form>

        </div>
    )
}
