import React from "react";

export const AvailableBalance = () => {
    return (
        <div className="available-balance-container">
            <div className="card">
                <h2>SALDO DISPONIBLE</h2>
                <span>$1,499,970.00</span>
            </div>
        </div>
    )
}
