import { Transaction } from "./Transaction";
import list from '../../../data/DataTransactions';

export const Transactions = () => {
    console.log("list", list)
    return (
        <div className="transactions-container">
            <div className="region-title">
                <h2>Tus Movimientos</h2>
                <button>Ver todos</button>
            </div>
            
            {
                list.map((e, i) =>{
                    return <Transaction key={i} element={e} />
                })
            }
        </div>
    );
}