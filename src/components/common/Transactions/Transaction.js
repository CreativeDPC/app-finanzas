export const Transaction = ({element}) => {
    const {name, category, date, amount, img} = element;
    
    return (
        <div className="transaction-container">
            <div id="logo-transaction">
                <img src={`./assets/images/${img}`} />                
            </div>
            <div id="info-transaction">
                <div>
                    <span className="bold">{name}</span>
                    <span>{date}</span>
                </div>
                <div>
                    <span>{category}</span>
                    <span className="bold">{amount}</span>
                </div>
            </div>
        </div>
    );
}